# The MIT License
#
# Copyright 2012 Sony Mobile Communications. All rights reserved.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" Gerrit SSH Client. """

from os.path import abspath, expanduser, isfile
import re
import six
import socket
import socks
from threading import Event, Lock

from .error import GerritError

from paramiko import SSHClient, SSHConfig, Transport
from paramiko.ssh_exception import SSHException
from paramiko import AutoAddPolicy


def _extract_version(version_string, pattern):
    """ Extract the version from `version_string` using `pattern`.

    Return the version as a string, with leading/trailing whitespace
    stripped.

    """
    if version_string:
        match = pattern.match(version_string.strip())
        if match:
            return match.group(1)
    return ""


class GerritSSHCommandResult(object):

    """ Represents the results of a Gerrit command run over SSH. """

    def __init__(self, command, stdin, stdout, stderr):
        self.command = command
        self.stdin = stdin
        self.stdout = stdout
        self.stderr = stderr

    def __repr__(self):
        return "<GerritSSHCommandResult [%s]>" % self.command


class GerritSSHClient(SSHClient):

    """ Gerrit SSH Client, wrapping the paramiko SSH Client. """

    def __init__(self, hostname, username=None, port=None, keepalive=None, 
                 key_filename=None, proxy_type=None, proxy_host=None, 
                 proxy_port=None):
        """ Initialise and connect to SSH. """
        super(GerritSSHClient, self).__init__()
        self.remote_version = None
        self.hostname = hostname
        self.username = username
        self.key_filename = key_filename
        self.port = port
        self.connected = Event()
        self.lock = Lock()
        self.proxy_type = proxy_type
        self.proxy_host = proxy_host
        self.proxy_port = proxy_port
        self.keepalive = keepalive
        self.sock = None

    def _configure(self):
        if self.key_filename and not isfile(self.key_filename):
            raise GerritError("Identity file '%s' does not exist" %
                                self.key_filename)
        if self.proxy_type and self.proxy_type != 'socks':
            raise GerritError("unknown proxy_type, must be 'socks'")

    def _do_connect(self):
        """ Connect to the remote. """
        self.set_missing_host_key_policy(AutoAddPolicy())
        self.load_system_host_keys()
        self._configure()
        if self.proxy_type and self.proxy_type == "socks":
            self.sock = socks.socksocket()
            self.sock.set_proxy(
                proxy_type=socks.SOCKS4,
                addr=self.proxy_host,
                port=self.proxy_port,
            )
            self.sock.connect((self.hostname, self.port))
        try:
            self.connect(hostname=self.hostname,
                         port=self.port,
                         username=self.username,
                         key_filename=self.key_filename,
                         sock=self.sock)
        except socket.error as e:
            raise GerritError("Failed to connect to server: %s" % e)

        try:
            version_string = self._transport.remote_version
            pattern = re.compile(r'^.*GerritCodeReview_([a-z0-9-\.]*) .*$')
            self.remote_version = _extract_version(version_string, pattern)
        except AttributeError:
            self.remote_version = None

    def _connect(self):
        """ Connect to the remote if not already connected. """
        if not self.connected.is_set():
            try:
                self.lock.acquire()
                # Another thread may have connected while we were
                # waiting to acquire the lock
                if not self.connected.is_set():
                    self._do_connect()
                    if self.keepalive:
                        self._transport.set_keepalive(self.keepalive)
                    self.connected.set()
            except GerritError:
                raise
            finally:
                self.lock.release()

    def get_remote_version(self):
        """ Return the version of the remote Gerrit server. """
        if self.remote_version is None:
            result = self.run_gerrit_command("version")
            version_string = result.stdout.read().decode('utf-8')
            pattern = re.compile(r'^gerrit version (.*)$')
            self.remote_version = _extract_version(version_string, pattern)
        return self.remote_version

    def get_remote_info(self):
        """ Return the username, and version of the remote Gerrit server. """
        version = self.get_remote_version()
        return (self.username, version)

    def run_gerrit_command(self, command):
        """ Run the given command.

        Make sure we're connected to the remote server, and run `command`.

        Return the results as a `GerritSSHCommandResult`.

        Raise `ValueError` if `command` is not a string, or `GerritError` if
        command execution fails.

        """
        if not isinstance(command, six.string_types):
            raise ValueError("command must be a string")
        gerrit_command = "gerrit " + command

        # are we sending non-ascii data?
        try:
            gerrit_command.encode('ascii')
        except UnicodeEncodeError:
            gerrit_command = gerrit_command.encode('utf-8')

        self._connect()
        try:
            stdin, stdout, stderr = self.exec_command(gerrit_command,
                                                      bufsize=1,
                                                      timeout=None,
                                                      get_pty=False)
        except SSHException as err:
            raise GerritError("Command execution error: %s" % err)
        return GerritSSHCommandResult(command, stdin, stdout, stderr)
